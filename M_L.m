clear all
close all
% clear train_data
[Train_data Train_text]=xlsread('train.xlsx');
train_data=cell(length(Train_data(:,1))+1,length(Train_text(1,:)));
train_text=Train_text(2:length(Train_text(:,1)),:);
% U=cell(1,length(Train_data(1,:)));
for i=1:1:length(Train_data(:,1))
    for j=1:1:length(Train_data(1,:))
    train_data{i,j}=Train_data(i,j);
    if isnan(Train_data(i,j))==1
        train_data{i,j}=train_text{i,j};
    end
         
    end
end
U=train_text(:,11);
V=train_text(:,12);
% train_data(2:length(train_data(:,1)),:)=train_data(1:length(train_data(:,1))-1,:);
% train_data{1,:}=zeros(1,length(train_data(1,:)));
train_data(1:length(U(:,1)),11)=U(1:length(U(:,1)),1);
train_data(1:length(V(:,1)),12)=V(1:length(V(:,1)),1);
train_data(2:length(train_data(:,1)),:)=train_data(1:length(train_data(:,1))-1,:);
train_data(1,:)=Train_text(1,:);
cc=0;

Xx=train_data(2:length(train_data(:,1)),3:12);
% Y determination
Y=Train_data(:,2);
Xx(:,2)=[];
Xx(:,6)=[];
X=zeros(length(Xx(:,1)),length(Xx(1,:))+1);
for i=1:1:891
    if strcmp(Xx(i,2),'male')==1;
        X(i,2)=1;
    else
        X(i,2)=0;
    end
     if isempty(Xx{i,7})==1;
        X(i,7)=0;
     else
%         te=Xx{i,7};
%          X(i,7)=str2num(te(2:length(te)));
cc=0;
    te=Xx{i,7};

    if length(te)>2 && isspace(te(2))==1
         X(i,7)=0;
    end
%  Faut enlever les lignes qui merdent avec du 'F G76'
    for j=i+1:1:891

    nbmots=(length(te)+1)/4;
    if nbmots>=2
        
     tp=Xx{j,7};
     if strcmp(te,tp)==1
         cc=cc+1;
         if cc>nbmots-1
             cc=nbmots-1;
         end
          Xx{j,7}=te(4*cc+1:4*cc+3);
     end
    end
    end
    if nbmots>=2 & isspace(te(2))==0
     Xx{i,7}=te(1:3);
    end

      te=Xx{i,7};
      
X(i,7)=str2double(te(2:length(te)));
       if length(te)>2 && isspace(te(2))==1
         X(i,7)=0;
       end
         if length(te)<3
         X(i,7)=0;
     end
     end

    if strcmp(Xx(i,8),'S')==1;
        X(i,8)=1;
    elseif strcmp(Xx(i,8),'Q')==1;
        X(i,8)=2;
    else
         X(i,8)=3;
    end
end
for p=1:1:891
    if isnan(Train_data(p,6))==1
        Train_data(p,6)=0;
    end
end
X(:,1)=Train_data(:,3);
X(:,3)=Train_data(:,6);
X(:,4)=Train_data(:,7);
X(:,5)=Train_data(:,8);
X(:,6)=Train_data(:,10);

% Xx(:,4)=[];
Alphabet=['A';'B';'C' ;'D' ;'E' ;'F' ;'G' ;'H' ;'I';'J';'K';'L';'M';'N';'O';'P';'Q';'R';'S';'T';'U';'V';'W';'X';'Y';'Z'];
% Numbers=['1';'2';'3';'4';'5';'6';'7';'8';'9'];
c=0;
id=0;
for p=2:1:length(Train_text(:,1))
    if isempty(Train_text{p,11})==1
        s=0;
    else
        s=Train_text{p,11};
    end
    d=s(1)==Alphabet;
    id=find(d);
      c=c+1;
    if isempty(id)==1
        id=0;
          
    end
  Col(c,1)=id;

end
X(:,9)=Col(:,1);
m=length(X(:,1));

X=[ones(m,1) X];
n=length(X(1,:));
theta=zeros(n,1);
lambda=0.00;
alpha=1E-5;
%  Feature scaling on columns4 and 7 

X(:,4)=(X(:,4)-mean(X(:,4)))/std(X(:,4));
X(:,7)=(X(:,7)-mean(X(:,7)))/std(X(:,7));
X(:,8)=(X(:,8)-mean(X(:,8)))/std(X(:,8));


% Cost computation
% X= ones Pclass Sex Age Sibsp Parch Fare Cabin number Embarkation Deck Number
noiterations=10000;
%  Gradient computation
Theta1=0;
% theta determination
for i=1:1:noiterations
%     J=(1/m)*(X*theta-Y)'*(X*theta-Y)+lambda*theta(2:n,1)'*theta(2:n,1);
  J=(-1/m)*(Y'*log(sigmoid(X*theta))+(ones(m,1)-Y)'*log(ones(m,1)-sigmoid(X*theta)))+lambda*theta(2:n,1)'*theta(2:n,1);
  if noiterations>=noiterations*0.95
    display(num2str(J))
  end
    grad=(2/m)*((sigmoid(X*theta-Y))'*X)';
    theta=theta-alpha*grad;
    if norm(Theta1-theta,2)/min(abs(Theta1))<0.05
        display('Algorithm converged')
        break
    end
    Theta1=theta;
end
% X validation set

% Test set
[Test_data Test_text]=xlsread('test.xlsx');
Xtest=zeros(length(Test_text(:,1)),length(Test_text(1,:))+1);
Test_data(:,1)=[];
Test_data(:,2)=[];
Test_data(:,6)=[];
Test_text(:,1)=[];
Test_text(:,2)=[];
Test_text(:,6)=[];

for i=1:1:418
    if strcmp(Test_text(i+1,2),'male')==1;
        Xtest(i,2)=1;
    else
        Xtest(i,2)=0;
    end
     if isempty(Test_text(i+1,7))==1;
        Xtest(i,7)=0;
     else
        tee=Test_text(i+1,7);
         Xtest(i,7)=str2num(tee(2:length(tee)));
     end
    if strcmp(Test_text(i+1,8),'S')==1;
        Xtest(i,8)=1;
    elseif strcmp(Test_text(i+1,8),'Q')==1;
        Xtest(i,8)=2;
    else
         Xtest(i,8)=3;
    end
end
for p=1:1:418
    if isnan(Test_data(p,3))==1
        Test_data(p,3)=0;
    end
end
% X(:,1)=Train_data(:,3);
% X(:,3)=Train_data(:,6);
% X(:,4)=Train_data(:,7);
% X(:,5)=Train_data(:,8);
% X(:,6)=Train_data(:,10);
% 
% % Xx(:,4)=[];
% Alphabet=['A';'B';'C' ;'D' ;'E' ;'F' ;'G' ;'H' ;'I'];
% c=0;
% id=0;
% for p=2:1:length(Train_text(:,1))
%     if isempty(Train_text{p,11})==1
%         s=0;
%     else
%         s=Train_text{p,11};
%     end
%     d=s(1)==Alphabet;
%     id=find(d);
%       c=c+1;
%     if isempty(id)==1
%         id=0;
%           
%     end
%   Col(c,1)=id;
% 
% end
% X(:,9)=Col(:,1);
% m=length(X(:,1));
% 
% X=[ones(m,1) X];


%  Learning curve parts
    